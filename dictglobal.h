#ifndef DICTGLOBAL_H
#define DICTGLOBAL_H

#include <stddef.h>

#ifdef __cplusplus
namespace jnp1 {

    extern "C" {
#endif

    /**
     * Zwraca identyfikator globalnego słownika, którego nie można usunąć.
     */
    unsigned long dict_global();

    /**
     * Stała jest równa 42 i określa maksymalny rozmiar globalnego słownika.
     * Do globalnego słownika można wstawiać kolejne klucze
     * z wartościami tylko, gdy jego rozmiar po wstawieniu nie będzie
     * przekraczał maksymalnego rozmiaru.
     */
    const size_t MAX_GLOBAL_DICT_SIZE = 42;

#ifdef __cplusplus
    }
}
#endif

#endif //_DICTGLOBAL_H
