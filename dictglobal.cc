#include "dictglobal.h"
#include "dict.h"

extern const bool debug;

unsigned long jnp1::dict_global() {
    static unsigned long globalId = jnp1::dict_new();

    return globalId;
}

