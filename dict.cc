#include "dictglobal.h"
#include "dict.h"
#include <unordered_map>
#include <vector>
#include <string>
#include <iostream>

#ifdef NDEBUG
const bool debug = false;
#else
const bool debug = true;
#endif

#define DEBUG(A) if(debug) { \
    cerr<<(A)<<"\n"; \
}

using namespace std;
using Dictionary = unordered_map<string, string>;
using Index = unsigned long;

static unordered_map<Index, Dictionary> &dictionaries() {
    static unordered_map<Index, Dictionary> dictionaries;
    return dictionaries;
};

Index globalDictIndex = jnp1::dict_global();

Index idCounter = 0;

///konwertuje Index słownika na stringa używanego w logowaniu.
static string dictionary_name(Index id) {
    return id != globalDictIndex ? to_string(id) : "the Global Dictionary";
}

///Konwertuje nazwę funkcji na prefix logu.
static string function_name(const string &functionName, Index id) {
    return functionName + ": dict " + dictionary_name(id);
}

///Konwertuje nazwę funkcji i jej argumenty na string z logiem wywołania.
static string function_call(
        const string &functionName,
        const vector<string> &args
) {
    string parsedArgs;
    bool isArg = false;
    for (const auto &arg : args) {
        isArg = true;
        parsedArgs.append(arg);
        parsedArgs.append(", ");
    }
    if (isArg) {
        parsedArgs.pop_back();
        parsedArgs.pop_back();
    }
    return functionName + "(" + parsedArgs + ")";
}

///Konwertuje wskaźnik na char do stringa używanego w logowaniu.
static string ptr_to_str(const char *ptr) {
    return ptr == nullptr ? "NULL" : ptr;
}



Index jnp1::dict_new() {
    static const string DICT_NEW = "dict_new";
    DEBUG(function_call(DICT_NEW, vector<string>()))

    dictionaries()[idCounter] = Dictionary();
    DEBUG(function_name(DICT_NEW, idCounter) + " has been created")

    return idCounter++;
}

void jnp1::dict_delete(Index id) {
    static const string DICT_DELETE = "dict_delete";
    DEBUG(function_call(DICT_DELETE, vector<string>({to_string(id)})))
    if (id == globalDictIndex) {
        DEBUG(DICT_DELETE + ", an attempt to remove " + dictionary_name(id))
    } else {
        auto it = dictionaries().find(id);
        if (it != dictionaries().end()) {
            dictionaries().erase(it);
            DEBUG(function_name(DICT_DELETE, id) + " has been deleted")
        } else {
            DEBUG(function_name(DICT_DELETE, id) + " not found")
        }
    }
}

size_t jnp1::dict_size(Index id) {
    static const string DICT_SIZE = "dict_size";
    DEBUG(function_call(DICT_SIZE, vector<string>({to_string(id)})))

    size_t result;
    auto it = dictionaries().find(id);
    if (it != dictionaries().end()) {
        result = it->second.size();
        DEBUG(function_name("dict_size", id)
              + " contains " + to_string(result) + " element(s)");
    } else {
        result = 0;
        DEBUG(function_name("dict_size", id) + " does not exist")
    }


    return result;
}

///sprawdza czy można wstawić nowy rekord do słownika globalnego
bool can_insert_to_global_dict(const string &key) {
    auto globalDictionary = dictionaries().find(globalDictIndex);
    unsigned long numberOfKeysInside = globalDictionary->second.count(key);

    return numberOfKeysInside != 0 ||
           globalDictionary->second.size() < jnp1::MAX_GLOBAL_DICT_SIZE;
}

void jnp1::dict_insert(Index id, const char *key, const char *value) {
    static const string DICT_INSERT = "dict_insert";
    DEBUG(function_call(
            DICT_INSERT,
            vector<string>({to_string(id), ptr_to_str(key), ptr_to_str(value)})
    ))

    auto dictionary = dictionaries().find(id);
    if (dictionary == dictionaries().end()) {

        DEBUG(function_name(DICT_INSERT, id) + " does not exist")
    } else if (value == nullptr) {

        DEBUG(function_name(DICT_INSERT, id) +
              ", an attempt to insert NULL value")
    } else if (key == nullptr) {

        DEBUG(function_name(DICT_INSERT, id) +
              ", an attempt to insert NULL key")
    } else {
        DEBUG(can_insert_to_global_dict(key))
        if (id != globalDictIndex || can_insert_to_global_dict(key)) {
            dictionary->second[key] = value;
            DEBUG(function_name(DICT_INSERT, id) + ", the pair ("
                  + key + ", " + value + ") has been inserted")
        } else {
            DEBUG(function_name(DICT_INSERT, id) +
                  " an attempt to insert new key into full dictionary")
        }
    }
}

void jnp1::dict_remove(Index id, const char *key) {
    static const string DICT_REMOVE = "dict_remove";
    DEBUG(function_call(DICT_REMOVE,
                        vector<string>({to_string(id), ptr_to_str(key)})))

    auto givenDictionary = dictionaries().find(id);
    if (key == nullptr) {

        DEBUG(function_name(DICT_REMOVE, id) + " key cannot be NULL")
    } else if (givenDictionary != dictionaries().end()) {

        auto foundKey = givenDictionary->second.find(key);
        if (foundKey == givenDictionary->second.end()) {
            string const NOT_FOUND = " does not contain the key ";
            DEBUG(function_name(DICT_REMOVE, id) + NOT_FOUND + key)
        } else {
            givenDictionary->second.erase(foundKey);
            DEBUG(function_name(DICT_REMOVE, id)
                  + ", the key " + key + " has been removed")
        }
    } else {

        DEBUG(function_name(DICT_REMOVE, id) + " does not exist")
    }
}

///szuka rekordu w danym słowniku
const char *find_in_dictionary(
        const char *key,
        const Dictionary &dict,
        Index id
) {
    static const string DICT_FIND = "dict_find";
    auto it = dict.find(string(key));
    if (it != dict.end()) {

        DEBUG(function_name(DICT_FIND, id) + ", the key " + string(key)
              + " has the value " + it->second)
        return it->second.c_str();
    } else {

        DEBUG(function_name(DICT_FIND, id)
              + ", the key \"" + string(key) + "\" not found")
        return nullptr;
    }
}

const char *jnp1::dict_find(Index id, const char *key) {
    static const string DICT_FIND = "dict_find";
    DEBUG(function_call(DICT_FIND,
                        vector<string>({to_string(id), ptr_to_str(key)})))

    auto it = dictionaries().find(id);
    if (key == nullptr) {

        DEBUG(function_name(DICT_FIND, id) + " key cannot be NULL")
        return nullptr;
    } else if (it != dictionaries().end()) {

        const char *res = find_in_dictionary(key, it->second, id);
        if (res != nullptr) {
            return res;
        }
    } else {

        DEBUG(function_name("dict_find", id) + " does not exist")
    }

    DEBUG(DICT_FIND + ": looking up the " + dictionary_name(globalDictIndex))
    const char *res = find_in_dictionary(
            key,
            dictionaries()[globalDictIndex],
            globalDictIndex
    );
    if (res != nullptr) {
        return res;
    }

    return nullptr;
}

void jnp1::dict_clear(Index id) {
    static const string DICT_CLEAR = "dict_clear";
    DEBUG(function_call(DICT_CLEAR, vector<string>({to_string(id)})))

    auto it = dictionaries().find(id);
    if (it != dictionaries().end()) {
        it->second.clear();
        DEBUG(function_name(DICT_CLEAR, id) + " has been cleared")
    }
}

void jnp1::dict_copy(Index src_id, Index dst_id) {
    static const string DICT_COPY = "dict_copy";
    DEBUG(function_call(DICT_COPY,
                        vector<string>({to_string(src_id), to_string(dst_id)})))

    auto src = dictionaries().find(src_id);
    auto dst = dictionaries().find(dst_id);
    if (src != dictionaries().end() && dst != dictionaries().end()) {
        for (auto i: src->second) {
            if (dst_id != globalDictIndex || (
                    dst_id == globalDictIndex &&
                    can_insert_to_global_dict(i.first))) {
                dst->second[i.first] = i.second;
            }
        }
        DEBUG(function_name(DICT_COPY, src_id) + " has been copied to dict "
              + to_string(dst_id))
    }
}